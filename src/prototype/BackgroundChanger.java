package prototype;

import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIFunctionMapper;
import com.sun.jna.win32.W32APITypeMapper;

import java.util.HashMap;

public class BackgroundChanger {
    BackgroundChanger(){
        //Constructor
    }

    public void setDesktopBackground(String imagePath){
        SPI.INSTANCE.SystemParametersInfo(
                new LongByReference(SPI.SPI_SETDESKTOPWALLPAPER),
                new IntByReference(0),
                imagePath,
                new LongByReference(SPI.SPIF_SENDWININICHANGE | SPI.SPIF_UPDATEINFIFILE)
        );
    }

    public interface SPI extends StdCallLibrary {

        long SPI_SETDESKTOPWALLPAPER = 20;
        long SPIF_UPDATEINFIFILE = 0x01;
        long SPIF_SENDWININICHANGE = 0x02;

        SPI INSTANCE = (SPI) Native.loadLibrary("user32", SPI.class, new HashMap<Object, Object>() {
            {
                put(OPTION_TYPE_MAPPER, W32APITypeMapper.UNICODE);
                put(OPTION_FUNCTION_MAPPER, W32APIFunctionMapper.UNICODE);
            }
        });

        boolean SystemParametersInfo(
                LongByReference uiAction,
                IntByReference uiParam,
                String pvParam,
                LongByReference fWinIni
        );
    }
}
